require 'sinatra'
require 'json'
require_relative './model/calculator'

get '/' do
  content_type :json
  {:success => true}.to_json
end

def is_float?(fl)
  !!Float(fl) rescue false
end

def validate_params_total
  errors = []

  if (!is_float?(params['amount']))
    errors.push(:invalid_amount)
  end

  if (!is_float?(params['price']))
    errors.push(:invalid_price)
  end

  if (!Calculator.new.validate_state(params['state']))
    errors.push(:invalid_state)
  end

  errors
end

def calculate_total
  amount = params['amount'].to_f
  price = params['price'].to_f
  state = params['state']    
  Calculator.new.total(amount, price, state)  
end

get '/calculate/total' do
  errors = validate_params_total()

  if (!errors.empty?) 
    response = {:errors => errors}
  else
    response = {:result => calculate_total()}
  end

  content_type :json
  response.to_json
end