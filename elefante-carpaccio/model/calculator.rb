class Calculator

  DISCOUNT_BY_BASE_TOTAL = {
    -Float::INFINITY..999 => 1.0,
    1000..4999 => 0.97,
    5000..6999 => 0.95,
    7000..9999 => 0.93,
    10000..49999 => 0.90,
    50000..Float::INFINITY => 0.85
  }

  TAX_BY_STATE = {
    'UT' => 1.0685,
    'NV' => 1.08,
    'TX' => 1.0625,
    'AL' => 1.04,
    'CA' => 1.0825
  }

  private def get_applicable_discount(base_total)
    DISCOUNT_BY_BASE_TOTAL.detect{|range, discount| range === base_total}.last
  end

  private def get_applicable_tax(state)
    TAX_BY_STATE[state]
  end

  def total(amount, price, state)
    base_total = amount * price
    applicable_discount = get_applicable_discount(base_total)
    applicable_tax = get_applicable_tax(state)

    # Since we're talking about money, we need to round to 2 decimals.
    (base_total * applicable_discount * applicable_tax).round(2)
  end

  def validate_state(potential_state) 
    TAX_BY_STATE.has_key?(potential_state)
  end

end