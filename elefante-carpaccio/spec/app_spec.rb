require_relative './spec_helper.rb'

describe "Sinatra Application" do
  
  it "get / should return success" do
    get '/'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:success => true}.to_json)
  end

  it "get /calculate/total?amount=26&price=172&state=CA should return 4695.71" do
    get '/calculate/total?amount=26&price=172&state=CA'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:result => 4695.71}.to_json)
  end

  it "get /calculate/total without state should return an error" do
    get '/calculate/total?amount=26&price=172'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:errors => [:invalid_state]}.to_json)
  end

  it "get /calculate/total without price should return an error" do
    get '/calculate/total?amount=26&state=CA'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:errors => [:invalid_price]}.to_json)
  end

  it "get /calculate/total with a non-numeric price should return an error" do
    get '/calculate/total?amount=26&price=1a9&state=CA'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:errors => [:invalid_price]}.to_json)
  end

  it "get /calculate/total without amount should return an error" do
    get '/calculate/total?price=19&state=CA'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:errors => [:invalid_amount]}.to_json)
  end

  it "get /calculate/total with a non-numeric amount should return an error" do
    get '/calculate/total?amount=2a6&price=19&state=CA'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:errors => [:invalid_amount]}.to_json)
  end

  it "get /calculate/total with multiple failed validations should inform all errors" do
    get '/calculate/total?amount=2a6&price=NaN&state=P'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:errors => [:invalid_amount, :invalid_price, :invalid_state]}.to_json)
  end

end