require_relative '../model/calculator'

describe "Calculator" do
  
  let(:calculator) { Calculator.new }  

  it "total for 10 items priced at $10 in UT should be 106.85" do
    expect(calculator.total(10, 10,'UT')).to eq 106.85
  end

  it "total for 8 items priced at $12 in UT should be 102.58" do
    expect(calculator.total(8, 12, 'UT')).to eq 102.58
  end

  it "total for 10 items priced at $10 in NV should be 108" do
    expect(calculator.total(10, 10, 'NV')).to eq 108
  end

  it "total for 10 items priced at $10 in TX should be 106.25" do
    expect(calculator.total(10, 10, 'TX')).to eq 106.25
  end

  it "total for 10 items priced at $10 in AL should be 104" do
    expect(calculator.total(10, 10, 'AL')).to eq 104
  end

  it "total for 10 items priced at $10 in CA should be 108.25" do
    expect(calculator.total(10, 10, 'CA')).to eq 108.25
  end

  it "total for 100 items priced at $10 in AL should be 1008.8 (3\% discount)" do
    expect(calculator.total(100, 10, 'AL')).to eq 1008.8
  end

  it "total for 25 items priced at $200 in AL should be 4940 (5\% discount)" do
    expect(calculator.total(25, 200, 'AL')).to eq 4940
  end

  it "total for 5 items priced at $1400 in AL should be 6770.4 (7\% discount)" do
    expect(calculator.total(5, 1400, 'AL')).to eq 6770.4
  end

  it "total for 1250 items priced at $8 in AL should be 9360 (10\% discount)" do
    expect(calculator.total(1250, 8, 'AL')).to eq 9360
  end

  it "total for 400 items priced at $125 in AL should be 44200 (15\% discount)" do
    expect(calculator.total(400, 125, 'AL')).to eq 44200
  end

end