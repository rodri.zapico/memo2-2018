 require 'rspec' 
require_relative '../model/isbn'

describe 'ISBN10' do

  let(:isbn) { ISBN10.new }  
   
  it '0471958697 debe ser un ISBN-10 valido' do
    expect(isbn.validate("0471958697")).to eq ISBN10::VALID
  end
  
  it 'string de menos de 10 caracteres debe ser demasiado corto' do
    expect(isbn.validate("58697")).to eq [ISBN10::TOO_SHORT]
  end

  it 'string de mas de 10 caracteres debe ser demasiado largo' do
    expect(isbn.validate("04719586971")).to eq [ISBN10::TOO_LONG]
  end

  it 'string de largo correcto con caracteres no numericos debe ser invalido' do
    expect(isbn.validate("04719A8697")).to eq [ISBN10::INVALID_CHARACTERS]
  end

  it 'string de largo correcto con X al final debe ser valido' do
    expect(isbn.validate("155404295X")).to eq ISBN10::VALID
  end

  it 'string con espacios adicionales debe ser valido' do
    expect(isbn.validate("155 404 295 X")).to eq ISBN10::VALID
  end

  it 'string con - adicionales debe ser valido' do
    expect(isbn.validate("155-404-295--X")).to eq ISBN10::VALID
  end

  it 'string con largo y caracteres invalidos debe informar ambos errores' do
    expect(isbn.validate("155A404295X")).to eq [ISBN10::TOO_LONG, ISBN10::INVALID_CHARACTERS]
  end

  it 'string con digito de check invalido debe ser invalido' do
    expect(isbn.validate("1554042951")).to eq [ISBN10::INVALID_CHECK_DIGIT]
    expect(isbn.validate("047195869X")).to eq [ISBN10::INVALID_CHECK_DIGIT]
  end

  it 'si el input no es un string, debe ser invalido' do
    expect(isbn.validate(1554042951)).to eq [ISBN10::NOT_A_STRING]
    expect(isbn.validate([1554042951])).to eq [ISBN10::NOT_A_STRING]
  end

  it '0471958697 y sus variaciones deben ser validos' do
    expect(isbn.validate("0471958697")).to eq ISBN10::VALID
    expect(isbn.validate("04 7195 8697")).to eq ISBN10::VALID
    expect(isbn.validate("04-7195-8697")).to eq ISBN10::VALID
    expect(isbn.validate("04-7195-869 7")).to eq ISBN10::VALID
  end

  it 'is_valid? deberia devolver true para los casos que validate devuelve VALID' do
    expect(isbn.is_valid?("0471958697")).to eq true
    expect(isbn.is_valid?("04 7195 8697")).to eq true
    expect(isbn.is_valid?("04-7195-8697")).to eq true
    expect(isbn.is_valid?("04-7195-869 7")).to eq true
    expect(isbn.is_valid?("155404295X")).to eq true    
  end

  it 'is_valid? deberia devolver false para los casos que validate devuelve un error' do
    expect(isbn.is_valid?("58697")).to eq false
    expect(isbn.is_valid?("04719586971")).to eq false
    expect(isbn.is_valid?("04719A8697")).to eq false    
    expect(isbn.is_valid?("047195869X")).to eq false
    expect(isbn.is_valid?(1554042951)).to eq false
  end

end
