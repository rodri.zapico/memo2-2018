class ISBN10

  IS_VALID_RESULTS = [
    VALID = 0, 
    TOO_SHORT = 1, 
    TOO_LONG = 2,
    INVALID_CHARACTERS = 3,
    INVALID_CHECK_DIGIT = 4,
    NOT_A_STRING = 5
  ]

  private def trim(input)
    return input.tr(" -", "")
  end

  private def validate_length(input)
    if input.length > 10
      return TOO_LONG
    elsif input.length < 10
      return TOO_SHORT
    else
      return VALID
    end
  end

  private def validate_characters(input)
    return /^\d+X?$/.match(input) ? VALID : INVALID_CHARACTERS
  end

  private def validate_format(input)
    rules_results = [validate_length(input), validate_characters(input)]
    errors = []

    rules_results.each{|rule_result|
      if rule_result != VALID
        errors.push(rule_result)
      end
    }

    return errors.length == 0 ? VALID : errors
  end

  private def validate_check_digit(input)
    idx = 0
    sum = 0
    errors = []

    while idx < 9 do
      sum += input[idx].to_i * (idx + 1)
      idx += 1
    end

    modulo = sum % 11

    if (modulo != 10)
      result = (modulo == input[9].to_i)
    else
      result = (input[9] == "X")
    end
        
    return result ? VALID : [INVALID_CHECK_DIGIT]
  end

  def validate(input)
    if input.class != String
      return [NOT_A_STRING]
    end

    input = trim(input)
    format_result = validate_format(input)
    check_digit_result = validate_check_digit(input)

    if format_result != VALID
      return format_result
    elsif check_digit_result != VALID
      return check_digit_result
    else
      return VALID
    end
  end

  def is_valid?(input)
    return validate(input) == VALID
  end

end
