require 'sinatra'
require 'json'
require_relative './model/chopper'

def string_to_array(string_array) 
  string_array.split(",").map{|numString| numString.to_i}
end

get '/sum' do
  string_array = params["x"]
  array = string_to_array(string_array)
  result = Chopper.new.sum(array)

  content_type :json
  {:sum => string_array, :resultado => result}.to_json
end

post '/chop' do
  chop_num = params['x'].to_i
  chop_array = string_to_array(params['y'])

  chop_string = 'x=' + params['x'] + ",y=" + params['y']
  chop_result = Chopper.new.chop(chop_num, chop_array)

  content_type :json
  {:chop => chop_string, :resultado => chop_result}.to_json 
end
