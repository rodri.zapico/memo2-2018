class Chopper

  def chop(n, array)
    # este find_index lo puedo hacer porque en los tests nunca se pasa
    # algo que no sea un array, con lo cual no va a pinchar.    
    result = array.find_index(n)
    return result ? result : -1
  end

  def sum(array)
    # Idem el find_index de arriba.
    number_sum = array.reduce(0, :+)
    if array.length == 0
      return "vacio"
    elsif number_sum >= 100
      return "demasiado grande"
    else
      # Debe haber una mejor forma de hacer este pedazo de codigo
      # pero no se me ocurrio, asique queda asi aunque sea medio feo
      traduccion_numero = {
        0 => "cero", 
        1 => "uno", 
        2 => "dos", 
        3 => "tres", 
        4 => "cuatro", 
        5 => "cinco", 
        6 => "seis", 
        7 => "siete", 
        8 => "ocho", 
        9 => "nueve"
      }

      result = ""
      integer_part = number_sum / 10
      decimal_part = number_sum % 10

      if integer_part > 0 
        result += traduccion_numero[integer_part]
        result += ","
      end

      result += traduccion_numero[decimal_part]

      return result
    end
  end

end
