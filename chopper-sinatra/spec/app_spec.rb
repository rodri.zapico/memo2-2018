require_relative './spec_helper.rb'

describe "Sinatra Application" do
  
  it "get /sum?x=9,9 should return 'uno', 'ocho'" do
    get '/sum?x=9,9'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:sum => "9,9", :resultado => "uno,ocho"}.to_json)
  end

  it "get /sum?x=1,9 should return 'uno', 'cero'" do
    get '/sum?x=1,9'
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:sum => "1,9", :resultado => "uno,cero"}.to_json)
  end

  it "post /chop should return the right position" do
    post "/chop", {x: "3", y:"0,7,3"}
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:chop => "x=3,y=0,7,3", :resultado => 2}.to_json)
  end

  it "post /chop of value not in the array should return -1" do
    post "/chop", {x: "1", y:"0,7,3"}
    expect(last_response).to be_ok
    expect(last_response.body).to eq ({:chop => "x=1,y=0,7,3", :resultado => -1}.to_json)
  end

end