require 'rspec' 
require_relative '../model/course'

describe 'Course' do

  let(:course) { Course.new }  
   
  it 'Add student should exist' do
    course.add_student('Juan')
  end

  it 'Add individual task should exist' do
    course.add_individual_task('ta')
  end

  it 'Add group task should exist' do
    course.add_group_task('gt')
  end  

  it 'Get individual tasks on course with no tasks should return empty array' do
    expect(course.get_individual_tasks()).to eq []
  end

  it 'Get individual tasks on course with a single task should return array with that tasks\' name' do
    course.add_individual_task('ta')
    expect(course.get_individual_tasks()).to eq ['ta']
  end

  it 'Get individual tasks on course with multiple tasks should return array with those tasks\' names' do
    course.add_individual_task('ta')
    course.add_individual_task('tb')
    expect(course.get_individual_tasks()).to eq ['ta', 'tb']
  end

  it 'Adding the same task multiple times should should add it only once' do
    course.add_individual_task('ta')
    course.add_individual_task('ta')
    expect(course.get_individual_tasks()).to eq ['ta']
  end  

  it 'Add individual task result should exist' do
    course.add_individual_task_result('ta', 'Juan', Course::TASK_PASSED)
  end  

  it 'Add individual task result for inexistant task should fail' do
    result = course.add_individual_task_result('ta', 'Juan', Course::TASK_PASSED)
    expect(result).to eq false
  end

  it 'Add individual task result for inexistant student should fail' do
    course.add_individual_task('ta')
    result = course.add_individual_task_result('ta', 'Juan', Course::TASK_PASSED)
    expect(result).to eq false
  end  

  it 'Add individual task result with the correct params should succeed' do
    course.add_individual_task('ta')
    course.add_student('Juan')
    result = course.add_individual_task_result('ta', 'Juan', Course::TASK_PASSED)
    expect(result).to eq true
  end

  it 'present(studentName) should exist' do
    course.add_student('Juan')
    course.present('Juan', true)
  end  

  it 'saying an unknown student is present should fail' do
    expect(course.present('Pepe', true)).to eq false
  end

  it 'saying an unknown student is not present should fail' do
    expect(course.present('Pepe', false)).to eq false
  end  

  it 'saying a known student is present should succeed' do
    course.add_student('Juan')
    expect(course.present('Juan', true)).to eq true
  end

  it 'Get group tasks on course with no tasks should return empty array' do
    expect(course.get_group_tasks()).to eq []
  end

  it 'Get group tasks on course with a single task should return array with that tasks\' name' do
    course.add_group_task('gt')
    expect(course.get_group_tasks()).to eq ['gt']
  end

  it 'Get group tasks on course with multiple tasks should return array with those tasks\' names' do
    course.add_group_task('gt1')
    course.add_group_task('gt2')
    expect(course.get_group_tasks()).to eq ['gt1', 'gt2']
  end

  it 'Adding the same task multiple times should should add it only once' do
    course.add_group_task('gt')
    course.add_group_task('gt')
    expect(course.get_group_tasks()).to eq ['gt']
  end

  it 'asking if a student passed an unknown task should fail' do
    course.add_student('Juan')
    expect(course.passed_task?('ta', 'Juan'))
  end

  it 'asking if an unknown student passed a task should fail' do
    course.add_individual_task("ta")
    expect(course.passed_task?('ta', 'Pepe'))
  end

  it 'asking if a passed student passed an individual task should succeed' do
    course.add_individual_task('ta')
    course.add_student('Juan')
    course.add_individual_task_result('ta', 'Juan', Course::TASK_PASSED)
    expect(course.passed_task?('ta', 'Juan')).to eq true
  end

  it 'asking if a passed student passed a group task should succeed' do
    course.add_group_task('gt')
    course.add_student('Juan')
    course.add_group_task_result('gt', 3, 'Juan', Course::TASK_PASSED)
    expect(course.passed_task?('gt', 'Juan')).to eq true
  end  

end
