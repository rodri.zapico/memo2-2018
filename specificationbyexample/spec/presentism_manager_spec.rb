require 'rspec' 
require_relative '../model/presentism_manager'

describe 'PresentismManager' do

  let(:manager) { PresentismManager.new }  
   
  it 'Add present student should exist' do
    manager.present('Juan', true)
  end

  it 'presentism of a student should be 0 if he was never present nor absent' do
    expect(manager.presentism('Juan')).to eq 0
  end

  it 'presentism of a student should be equal to the percentage of classes he attended' do
    manager.present('Juan', true)
    manager.present('Juan', true)
    manager.present('Juan', true)
    manager.present('Juan', false)
    manager.present('Juan', false)
    expect(manager.presentism('Juan')).to eq 60
  end

  it 'passed? should be true for a presentism of 75%' do
    manager.present('Juan', true)
    manager.present('Juan', true)
    manager.present('Juan', true)
    manager.present('Juan', false)
    expect(manager.passed?('Juan')).to eq true
  end

end
