require 'rspec' 
require_relative '../model/individual_task_manager'

describe 'IndividualTaskManager' do

  let(:task_manager) { IndividualTaskManager.new }  
   
  it 'Add result should exist' do
    task_manager.add_result('Juan', IndividualTaskManager::PASSED)
  end

  it 'passed? should return false for an unknown student' do
    expect(task_manager.passed?('Pepe')).to eq false
  end

  it 'passed? should return true for a passed student' do
    task_manager.add_result("Juan", IndividualTaskManager::PASSED)
    expect(task_manager.passed?('Juan')).to eq true
  end  

end
