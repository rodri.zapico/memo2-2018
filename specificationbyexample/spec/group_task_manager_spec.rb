require 'rspec' 
require_relative '../model/group_task_manager'

describe 'GroupTaskManager' do

  let(:manager) { GroupTaskManager.new }  
   
  it 'add_iteration_results should exist' do
    manager.add_iteration_results('Juan', GroupTaskManager::PASSED, 1)
  end

  it 'passed? should return false for an unknown student' do
    expect(manager.passed?('Pepe')).to eq false
  end

  it 'passed? should return true for a passed student' do
    manager.add_iteration_results('Juan', GroupTaskManager::PASSED, 3)
    expect(manager.passed?('Juan')).to eq true
  end

  it 'passed? should return false if a student only passed 2 iterations' do
    manager.add_iteration_results('Juan', GroupTaskManager::PASSED, 2)
    expect(manager.passed?('Juan')).to eq false
  end

end
