Feature: Approval regime

  Background:
    Given a student 'Juan' exists
    And an individual task 'ta' exists
    And an individual task 'tb' exists
    And an individual task 'tc' exists
    And a group task 'pg' exists

  Scenario: student passes 
    Given 'Juan' has passed individual task 'ta'
    And 'Juan' has passed individual task 'tb'
    And 'Juan' has passed individual task 'tc'
    And 'Juan' has 75% presentism
    And 'Juan' has passed 3 iterations of 'pg'
    Then 'Juan' has passed the course

  Scenario: student fails due to absentism
    Given 'Juan' has passed individual task 'ta'
    And 'Juan' has passed individual task 'tb'
    And 'Juan' has passed individual task 'tc'
    And 'Juan' has 60% presentism
    And 'Juan' has passed 3 iterations of 'pg'
    Then 'Juan' has failed the course

  Scenario: student fails due to individual tasks
    Given 'Juan' has passed individual task 'ta'
    And 'Juan' has passed individual task 'tb'
    And 'Juan' has failed individual task 'tc'
    Then 'Juan' has failed the course

  Scenario: student passes 
    Given 'Juan' has passed individual task 'ta'
    And 'Juan' has passed individual task 'tb'
    And 'Juan' has passed individual task 'tc'
    And 'Juan' has 75% presentism
    And 'Juan' has passed 2 iterations of 'pg'
    Then 'Juan' has failed the course    