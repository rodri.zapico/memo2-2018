def define_course_if_not_defined 
  if (!defined?(@course))     
    @course = Course.new
  end
end

Given(/^a student '(\w+)' exists$/) do |student_name|
  define_course_if_not_defined()
  @course.add_student(student_name)
end

Given(/^an individual task '(\w+)' exists$/) do |task_name|
  define_course_if_not_defined()
  @course.add_individual_task(task_name)
end

Given(/^a group task '(\w+)' exists$/) do |task_name|
  define_course_if_not_defined()
  @course.add_group_task(task_name)
end

Given(/^'(\w+)' has passed individual task '(\w+)'$/) do |student_name, task_name|
  @course.add_individual_task_result(task_name, student_name, Course::TASK_PASSED)
end

Given(/^'(\w+)' has (\d+)% presentism$/) do |student_name, presentism|
  @course.set_presentism(student_name, presentism)
end

Given(/^'(\w+)' has passed (\d+) iterations of '(\w+)'$/) do |student_name, num_iterations, group_task_name|
    @course.add_group_task_result(group_task_name, num_iterations.to_i, student_name, Course::TASK_PASSED)
end

Then(/^'(\w+)' has passed the course$/) do |student_name|
  expect(@course.passed?(student_name)).to eq true
end

Then(/^'(\w+)' has failed the course$/) do |student_name|
  expect(@course.passed?(student_name)).to eq false
end

Given(/^'(\w+)' has failed individual task '(\w+)'$/) do |student_name, failed_task_name|
  @course.add_individual_task_result(failed_task_name, student_name, Course::TASK_FAILED)
end
