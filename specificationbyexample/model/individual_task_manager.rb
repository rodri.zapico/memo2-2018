require_relative '../model/task_manager'

class IndividualTaskManager < TaskManager

  def initialize
    @students = {}
  end

  def add_result(student_name, result)
    @students[student_name] = result
  end

  def passed?(student_name)
    @students[student_name] == PASSED
  end

end