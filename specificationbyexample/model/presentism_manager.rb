class PresentismManager

  def initialize 
    @students = {}
  end

  def present(student_name, is_present)
    if (!@students.keys.include?(student_name)) 
      @students[student_name] = {}
      @students[student_name]['present'] = 0
      @students[student_name]['absent'] = 0
    end
    if is_present
      @students[student_name]['present'] += 1
    else 
      @students[student_name]['absent'] += 1
    end
  end

  def presentism(student_name)
    if (!@students.keys.include?(student_name))
      return 0
    end
    present = @students[student_name]["present"]
    absent = @students[student_name]["absent"]
    (present * 100 / (present + absent))
  end

  def passed?(student_name)
    presentism(student_name) >= 75
  end

end