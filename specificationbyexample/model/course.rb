require_relative '../model/individual_task_manager'
require_relative '../model/group_task_manager'
require_relative '../model/presentism_manager'

class Course  

  def initialize
    @students = []
    @individual_tasks = {}
    @group_tasks = {}
    @tasks = {}
    @presentism = PresentismManager.new
  end  

  def add_student(student_name)
    @students.push(student_name) unless @students.include?(student_name)
  end

  def add_individual_task(task_name)
    new_task = IndividualTaskManager.new
    @individual_tasks[task_name] = new_task
    @tasks[task_name] = new_task
  end

  def get_individual_tasks()
    @individual_tasks.keys
  end

  def add_group_task(task_name)
    new_task = GroupTaskManager.new
    @group_tasks[task_name] = new_task
    @tasks[task_name] = new_task
  end 

  def get_group_tasks()
    @group_tasks.keys
  end  

  TASK_RESULTS = [
    TASK_PASSED = 1,
    TASK_FAILED = 2
  ]

  def add_individual_task_result(task_name, student_name, result)
    if !@students.include?(student_name) || !@individual_tasks[task_name]
      return false
    end
    @individual_tasks[task_name].add_result(student_name, result)
    true
  end

  def add_group_task_result(task_name, num_its, student_name, result)
    if !@students.include?(student_name) || !@group_tasks[task_name]
      return false
    end
    @group_tasks[task_name].add_iteration_results(student_name, result, num_its)
    true    
  end  

  def passed_task?(task_name, student_name)
    task = @tasks[task_name]
    task ? task.passed?(student_name) : false
  end

  def passed_all_tasks?(student_name)
    passed_tasks = true
    @tasks.each_value { |task|
      passed_tasks = passed_tasks && task.passed?(student_name)
    }
    passed_tasks
  end

  def present(student_name, is_present)
    if !@students.include?(student_name)
      return false
    end
    @presentism.present(student_name, is_present)
    true
  end

  def passed?(student_name)
    (@presentism.passed?(student_name) && passed_all_tasks?(student_name))
  end

  def set_presentism(student_name, presentism)
    n = 0
    while n < presentism.to_i
      present(student_name, true)
      n += 1
    end

    while n < 100
      present(student_name, false)
      n += 1    
    end
  end    

end
