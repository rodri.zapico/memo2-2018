require_relative '../model/task_manager'

class GroupTaskManager < TaskManager

  def initialize
    @students = {}
  end

  def add_iteration_results(student_name, result, num_iterations)
    if (!@students.keys.include?(student_name)) 
      @students[student_name] = {}
    end
    @students[student_name][result] = num_iterations
  end

  def passed?(student_name)
    if !@students[student_name] || !@students[student_name][PASSED]
      return false
    end
    @students[student_name][PASSED] >= 3 
  end

end